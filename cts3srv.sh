#!/bin/sh
# POSIX

TS3PATH="/home/teamspeak"
TS3USER="teamspeak"

IGNORE_OS_CHECK=false

# colors
COLOR_RED='\033[0;31m'
COLOR_YELLOW='\033[0;33m'
COLOR_GREEN='\033[0;32m'
COLOR_NC='\033[0m'

update_upgrade() {
    echo -e "${COLOR_GREEN}Update Server ...${COLOR_NC}"
    apt-get -qq -y update
    #apt-get -qq -y upgrade
}

show_logo() {
    #http://patorjk.com/software/taag/#p=display&c=bash&f=Big&t=C%20TS3%20SRV%0A%2C.........................%2C%0A%20%20%20%20%20%20%20%20%20V%20.%201%20.%201%20.%201
cat << EOF
     _____   _______ _____ ____     _____ _______      __ 
    / ____| |__   __/ ____|___ \   / ____|  __ \ \    / / 
   | |         | | | (___   __) | | (___ | |__) \ \  / /  
   | |         | |  \___ \ |__ <   \___ \|  _  / \ \/ /   
   | |____     | |  ____) |___) |  ____) | | \ \  \  /    
    \_____| _ _|_|_|_____/|____/_ |_____/|_|_ \_\ _\/ _ _ 
   ( |_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_| )
   |/       \ \    / /     /_ |     /_ |     /_ |      |/ 
             \ \  / /       | |      | |      | |         
              \ \/ /        | |      | |      | |         
               \  /     _   | |  _   | |  _   | |         
                \/     (_)  |_| (_)  |_| (_)  |_|         
                                                          
                                                          
EOF
}

# Show help
show_help() {
cat << EOF
    -h|-help                    Display this help and exit
    -v|-version                 Show version of this script

    --ignore-os-check           Ignore OS check

    --create                    Create Teamspeak server
    --update                    Update Teamspeak server to the newest version
    --fix                       Fix Teamspeak server installation after errors
    --remove                    Remove Teamspeak server

    --start                     Start your Teamspeak server
    --status                    Get Status from your Teamspeak server
    --live-log                  Get Live-Log from your Teamspeak server
    --reboot                    Reboot your Teamspeak server
    --stop                      Stop your Teamspeak server

    --prikey                    Get ServerAdmin privilege key
    --server-query              Get Server Query username, password and apikey

    ====================================================================================
    ||                                  EXAMPLES:                                     ||
    ====================================================================================

    Create Teamspeak server                 bash cts3srv.sh [--ignore-os-check] --create
    Update Teamspeak server                 bash cts3srv.sh [--ignore-os-check] --update
    Fix Teamspeak server                    bash cts3srv.sh [--ignore-os-check] --fix
    Remove Teamspeak server                 bash cts3srv.sh [--ignore-os-check] --remove

    Start Teamspeak server                  bash cts3srv.sh --start
    Get Status from Teamspeak server        bash cts3srv.sh --status
    Get Live-Log from Teamspeak server      bash cts3srv.sh --live-log
    Reboot Teamspeak server                 bash cts3srv.sh --reboot
    Stop Teamspeak server                   bash cts3srv.sh --stop

    Get ServerAdmin privilege key           bash cts3srv.sh --prikey
    Get Server Query username, password     bash cts3srv.sh --server-query
    and apikey

EOF
}

# Show version
show_version() {
cat << EOF
                     Author: Justin Dittmer
                    Website: jdittmer.dev
                     GitLab: https://gitlab.com/JDittmerDEV/cts3srv

EOF
}

# Check OS system
check_operation_system(){
    egrep '^(VERSION_ID|NAME)=' /etc/os-release | cut -d'"' -f2 > cts3srv__op.txt

    op_name=$(head -n 1 cts3srv__op.txt)
    op_version=$(tail -n 1 cts3srv__op.txt | cut -d'.' -f1 | cut -d' ' -f1)

    opname=false
    opversion=false

    if [ "$op_name" == "Debian GNU/Linux" ]; then
        opname=true

        if [ "$op_version" == "10" ]; then
            opversion=true
        fi

        if [ "$op_version" == "11" ]; then
            opversion=true
        fi

        if [ "$op_version" == "12" ]; then
            opversion=true
        fi
    fi

    if [ "$op_name" == "Ubuntu" ]; then
        opname=true

        if [ "$op_version" == "16" ]; then
            opversion=true
        fi

        if [ "$op_version" == "18" ]; then
            opversion=true
        fi

<<com
        if [ "$op_version" == "20" ]; then
            opversion=true
        fi

        if [ "$op_version" == "22" ]; then
            opversion=true
        fi
com
    fi

    rm cts3srv__op.txt

    if [ $opname == false ]; then
        echo
        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The operating system is not supported!"
        echo
        exit
    fi

    if [ $opversion == false ]; then
        echo
        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: The version of the operating system is not supported!"
        echo
        exit
    fi
}

while :; do
    case $1 in
        -h|-help)     # Help
            reset
            show_logo
            show_help
            exit
            ;;
        -v|-version)  # Show last version
            reset
            show_logo
            show_version
            exit
            ;;
        --ignore-os-check)
            while true; do
                echo " "
                echo "Are you sure you want to continue despite your operating system is not supported?"
                echo "It can lead to errors and even corrupted files!"
                read -p "Continue [y|n]: " yn
                case $yn in
                    [Yy]*) IGNORE_OS_CHECK=true; break;;
                    [Nn]*) exit;;
                    *) echo "Please answer yes or no.";;
                esac
            done
            ;;
        --create)   # Create Teamspeak server
            reset
            show_logo

            if [ ! -d $TS3PATH ]; then
                if [ $IGNORE_OS_CHECK == false ]; then
                    check_operation_system
                fi

                update_upgrade

                if ! getent passwd "$TS3USER" > /dev/null 2>&1; then
                    echo -e "${COLOR_GREEN}Create Teamspeak user ...${COLOR_NC}"
                    adduser $TS3USER --gecos "" --disabled-password --disabled-login
                fi

                if [ ! -d $TS3PATH ]; then
                    echo -e "${COLOR_GREEN}Create Teamspeak folder ...${COLOR_NC}"
                    mkdir $TS3PATH
                fi

                echo -e "${COLOR_GREEN}Checking last Teamspeak server version ...${COLOR_NC}"
                curl -s https://files.teamspeak-services.com/releases/server/ | grep -oP '<a href=".+?">\K.+?(?=<)' | sort > cts3srv__latestversion.txt
                sort -V -o cts3srv__latestversion.txt cts3srv__latestversion.txt
                latestversion=$(tail -n 1 cts3srv__latestversion.txt)

                echo -e "${COLOR_GREEN}Download and unpack Teamspeak files ...${COLOR_NC}"
                wget -q https://files.teamspeak-services.com/releases/server/$latestversion/teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                tar xf teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                mv teamspeak3-server_linux_amd64/* $TS3PATH 2>/dev/null
                rm -rf teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                rm -r teamspeak3-server_linux_amd64
                rm cts3srv__latestversion.txt

                echo -e "${COLOR_GREEN}Accept Teamspeak Server License ...${COLOR_NC}"
                touch $TS3PATH/.ts3server_license_accepted

                echo " "
                echo -e "${COLOR_GREEN}Grant user and folder rights...${COLOR_NC}"
                chown -cR ${TS3USER}:${TS3USER} ${TS3PATH} > /dev/null

                echo -e "${COLOR_GREEN}Create teamspeak.service ...${COLOR_NC}"
                cat << EOF > /lib/systemd/system/teamspeak.service
[Unit]
Description=TeamSpeak 3 Server
After=network.target
[Service]
WorkingDirectory=$TS3PATH/
User=$TS3USER
Group=$TS3USER
Type=forking
ExecStart=$TS3PATH/ts3server_startscript.sh start inifile=ts3server.ini
ExecStop=$TS3PATH/ts3server_startscript.sh stop
PIDFile=$TS3PATH/ts3server.pid
RestartSec=15
Restart=always
[Install]
WantedBy=multi-user.target
EOF

                echo -e "${COLOR_GREEN}Release ports ...${COLOR_NC}"
                iptables -A INPUT -p udp --dport 9987 -j ACCEPT
                iptables -A INPUT -p tcp --dport 30033 -j ACCEPT
                iptables -A INPUT -p tcp --dport 10011 -j ACCEPT
                
                echo -e "${COLOR_GREEN}Enable and start teamspeak.service ...${COLOR_NC}"
                systemctl enable teamspeak.service
                systemctl start teamspeak.service

                while true; do
                    if [ $(systemctl show -p SubState --value teamspeak) == "running" ]; then
                        sleep 2
                        break
                    fi

                    if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                        reset
                        show_logo

                        echo " "
                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while creating the server!"
                        echo
                        journalctl -p err -b -u teamspeak
                        exit
                    fi
                done
                reset
                show_logo

                echo
                echo -e "Your Teamspeak server is now reachable on ${COLOR_YELLOW}$(hostname -I | cut -d' ' -f1)${COLOR_NC}"
                echo -e "Your ServerAdmin privilege key created: ${COLOR_YELLOW}$(cat $TS3PATH/logs/* | grep -o 'token=.*' | cut -d'=' -f2)${COLOR_NC}"
                echo
                echo "Your Server Query Admin Account datas:"
                echo "======================================"
                echo -e "Loginname: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'loginname= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo -e "Password: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'password= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo -e "Apikey: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'apikey= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: There already exists a Teamspeak server! Please delete the old one before creating a new one."
                echo
            fi
            ;;
        --update)   # Update Teamspeak server
            reset
            show_logo

            if [ $IGNORE_OS_CHECK == false ]; then
                check_operation_system
            fi

            if [ -d $TS3PATH ]; then
                curl -s https://files.teamspeak-services.com/releases/server/ | grep -oP '<a href=".+?">\K.+?(?=<)' | sort > cts3srv__latestversion.txt
                sort -V -o cts3srv__latestversion.txt cts3srv__latestversion.txt
                latestversion=$(tail -n 1 cts3srv__latestversion.txt)

                cat $TS3PATH/CHANGELOG | grep -o 'Server Release .*' | cut -d' ' -f3 > cts3srv__currentversion.txt
                currentversion=$(head -n 1 cts3srv__currentversion.txt)

                reset
                show_logo

                if [ $currentversion != $latestversion ]; then
                    echo
                    echo -e "[${COLOR_YELLOW}WARNING${COLOR_NC}]: Your Teamspeak server has an older version from Teamspeak!"
                    echo
                    echo -e "Your Teamspeak server has the version: ${COLOR_YELLOW}$currentversion${COLOR_NC}"
                    echo -e "Latest version from Teamspeak: ${COLOR_YELLOW}$latestversion${COLOR_NC}"
                    echo

                    while true; do
                        read -p "Would you like to update your Teamspeak server? [y|n]: " yn
                        case $yn in
                            [Yy]*)
                                # backup teamspeak server
                                echo -e "${COLOR_GREEN}Create backup from current Teamspeak server ...${COLOR_NC}"
                                currdate=$(date '+%m%d%Y-%H%M%S')
                                tar -czf /home/ts3_backup-$currentversion-$currdate.tar.gz $TS3PATH

                                echo -e "${COLOR_GREEN}Stop and disable teamspeak.service ...${COLOR_NC}"
                                systemctl stop teamspeak.service
                                systemctl disable teamspeak.service

                                update_upgrade

                                # update teamspeak server
                                echo -e "${COLOR_GREEN}Download and unpack new Teamspeak files ...${COLOR_NC}"
                                wget -q https://files.teamspeak-services.com/releases/server/$latestversion/teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                                tar xf teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                                mv teamspeak3-server_linux_amd64/* $TS3PATH 2>/dev/null
                                rm -rf teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                                rm -r teamspeak3-server_linux_amd64

                                update_upgrade

                                echo -e "${COLOR_GREEN}Release ports ...${COLOR_NC}"
                                iptables -A INPUT -p udp --dport 9987 -j ACCEPT
                                iptables -A INPUT -p tcp --dport 30033 -j ACCEPT
                                iptables -A INPUT -p tcp --dport 10011 -j ACCEPT

                                echo -e "${COLOR_GREEN}Enable and start teamspeak.service ...${COLOR_NC}"
                                systemctl enable teamspeak.service
                                systemctl start teamspeak.service

                                while true; do
                                    if [ $(systemctl show -p SubState --value teamspeak) == "running" ]; then
                                        sleep 2
                                        break
                                    fi

                                    if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                                        reset
                                        show_logo

                                        echo " "
                                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while creating the server!"
                                        echo
                                        journalctl -p err -b -u teamspeak
                                        exit
                                    fi
                                done

                                reset
                                show_logo
                                echo
                                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Teamspeak server is up to date!"
                                echo

                                break;;
                            [Nn]*) break;;
                            *) echo "Please answer yes or no.";;
                        esac
                    done
                else
                    echo
                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Teamspeak server is up to date!"
                    echo
                fi

                rm cts3srv__latestversion.txt
                rm cts3srv__currentversion.txt
            else
                echo
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Your Teamspeak server folder doesn't seem to exist."
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An update is therefore pointless at this point!"
                echo
            fi
            ;;
        --fix)      # Fix Teamspeak server installation, after errors
            reset
            show_logo

            if [ -d $TS3PATH ]; then
                if [ $IGNORE_OS_CHECK == false ]; then
                    check_operation_system
                fi

                while true; do
                    echo " "
                    echo "Are you sure you want to fix the server?"
                    echo "A backup will be created from the latest version and all current data will be overwritten!"
                    read -p "Continue [y|n]: " yn
                    case $yn in
                        [Yy]*) break;;
                        [Nn]*) exit;;
                        *) echo "Please answer yes or no.";;
                    esac
                done

                # backup teamspeak server
                echo -e "${COLOR_GREEN}Create backup from current Teamspeak server ...${COLOR_NC}"
                cat $TS3PATH/CHANGELOG | grep -o 'Server Release .*' | cut -d' ' -f3 > cts3srv__currentversion.txt
                currentversion=$(head -n 1 cts3srv__currentversion.txt)
                currdate=$(date '+%m%d%Y-%H%M%S')
                tar -czf /home/ts3_backup-$currentversion-$currdate.tar.gz $TS3PATH
                rm cts3srv__currentversion.txt

                # remove old files
                echo -e "${COLOR_GREEN}Stop and disable teamspeak.service ...${COLOR_NC}"
                systemctl stop teamspeak.service
                systemctl disable teamspeak.service

                echo -e "${COLOR_GREEN}Remove teamspeak.service ...${COLOR_NC}"
                rm /lib/systemd/system/teamspeak.service

                if getent passwd "$TS3USER" > /dev/null 2>&1; then
                    echo -e "${COLOR_GREEN}Remove Teamspeak user ...${COLOR_NC}"
                    deluser $TS3USER
                fi

                # download and install new
                if [ -d $TS3PATH ]; then
                    echo -e "${COLOR_GREEN}Create Teamspeak user ...${COLOR_NC}"
                    adduser $TS3USER --gecos "" --disabled-password --disabled-login
                else
                    echo -e "${COLOR_GREEN}Create Teamspeak user and teamspeak folder ...${COLOR_NC}"
                    adduser $TS3USER --gecos "" --disabled-password --disabled-login
                    mkdir $TS3PATH
                fi

                curl -s https://files.teamspeak-services.com/releases/server/ | grep -oP '<a href=".+?">\K.+?(?=<)' | sort > cts3srv__latestversion.txt
                sort -V -o cts3srv__latestversion.txt cts3srv__latestversion.txt
                latestversion=$(tail -n 1 cts3srv__latestversion.txt)

                echo -e "${COLOR_GREEN}Download and unpack Teamspeak files ...${COLOR_NC}"
                wget -q https://files.teamspeak-services.com/releases/server/$latestversion/teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                tar xf teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                mv teamspeak3-server_linux_amd64/* $TS3PATH 2>/dev/null
                rm -rf teamspeak3-server_linux_amd64-$latestversion.tar.bz2
                rm -r teamspeak3-server_linux_amd64
                rm cts3srv__latestversion.txt

                echo -e "${COLOR_GREEN}Accept Teamspeak Server License ...${COLOR_NC}"
                touch $TS3PATH/.ts3server_license_accepted

                echo -e "${COLOR_GREEN}Create teamspeak.service ...${COLOR_NC}"
                cat << EOF > /lib/systemd/system/teamspeak.service
[Unit]
Description=TeamSpeak 3 Server
After=network.target
[Service]
WorkingDirectory=$TS3PATH/
User=$TS3USER
Group=$TS3USER
Type=forking
ExecStart=$TS3PATH/ts3server_startscript.sh start inifile=ts3server.ini
ExecStop=$TS3PATH/ts3server_startscript.sh stop
PIDFile=$TS3PATH/ts3server.pid
RestartSec=15
Restart=always
[Install]
WantedBy=multi-user.target
EOF

                update_upgrade

                echo -e "${COLOR_GREEN}Release ports ...${COLOR_NC}"
                iptables -A INPUT -p udp --dport 9987 -j ACCEPT
                iptables -A INPUT -p tcp --dport 30033 -j ACCEPT
                iptables -A INPUT -p tcp --dport 10011 -j ACCEPT

                echo -e "${COLOR_GREEN}Enable and start teamspeak.service ...${COLOR_NC}"
                systemctl enable teamspeak.service
                systemctl start teamspeak.service

                while true; do
                    if [ $(systemctl show -p SubState --value teamspeak) == "running" ]; then
                        sleep 2
                        break
                    fi

                    if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                        reset
                        show_logo

                        echo " "
                        echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while fixing the server!"
                        echo
                        journalctl -p err -b -u teamspeak
                        exit
                    fi
                done
                reset
                show_logo
                echo
                echo -e "Your Teamspeak server is now reachable on ${COLOR_YELLOW}$(hostname -I | cut -d' ' -f1)${COLOR_NC}"
                echo -e "Your ServerAdmin privilege key: ${COLOR_YELLOW}$(cat $TS3PATH/logs/* | grep -o 'token=.*' | cut -d'=' -f2)${COLOR_NC}"
                echo
                echo
                echo "Your Server Query Admin Account datas:"
                echo "======================================"
                echo -e "Loginname: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'loginname= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo -e "Password: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'password= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo -e "Apikey: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'apikey= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo
            else
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: There is no Teamspeak server that you could fix or did I miss something?"
                echo
            fi
            ;;
        --remove)   # Remove Teamspeak server
            reset
            show_logo

            if [ $IGNORE_OS_CHECK == false ]; then
                check_operation_system
            fi

            update_upgrade

            echo -e "${COLOR_GREEN}Stop and disable teamspeak.service ...${COLOR_NC}"
            systemctl stop teamspeak.service
            systemctl disable teamspeak.service

            echo -e "${COLOR_GREEN}Remove teamspeak user and files ...${COLOR_NC}"
            rm /lib/systemd/system/teamspeak.service
            deluser $TS3USER
            rm -r $TS3PATH

            update_upgrade

            reset
            show_logo
            echo
            echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Teamspeak server including user and service has been deleted!"
            echo
            ;;
        --prikey)   # Get ServerAdmin Privilege key
            reset
            show_logo

            if [ -d $TS3PATH ]; then
                echo
                echo -e "Your ServerAdmin privilege key: ${COLOR_YELLOW}$(cat $TS3PATH/logs/* | grep -o 'token=.*' | cut -d'=' -f2)${COLOR_NC}"
                echo
            else
                echo
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Your Teamspeak server folder doesn't seem to exist."
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Therefore one could not find a Privilege Key!"
                echo
            fi
            ;;
        --server-query)   # Get Server Query username, password and apikey
            reset
            show_logo

            if [ -d $TS3PATH ]; then
                echo
                echo "Your Server Query Admin Account datas:"
                echo "======================================"
                echo -e "Loginname: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'loginname= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo -e "Password: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'password= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo -e "Apikey: ${COLOR_YELLOW}$(journalctl -u teamspeak | grep -o 'apikey= ".*' | tail -1 | cut -d'"' -f2)${COLOR_NC}"
                echo
            else
                echo
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Your Teamspeak server folder doesn't seem to exist."
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Therefore one could not find a Privilege Key!"
                echo
            fi
            ;;
        --start)   # Start Teamspeak server
            reset
            show_logo

            while true; do
                if [ $(systemctl show -p SubState --value teamspeak) == "running" ]; then
                    echo " "
                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Teamspeak server is already running!"
                    echo
                    exit
                fi

                if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                    echo -e "${COLOR_GREEN}Release ports ...${COLOR_NC}"
                    iptables -A INPUT -p udp --dport 9987 -j ACCEPT
                    iptables -A INPUT -p tcp --dport 30033 -j ACCEPT
                    iptables -A INPUT -p tcp --dport 10011 -j ACCEPT
                    
                    echo -e "${COLOR_GREEN}Starting teamspeak.service ...${COLOR_NC}"
                    systemctl start teamspeak.service
                    break
                fi
            done

            sleep 2

            while true; do
                if [ $(systemctl show -p SubState --value teamspeak) == "running" ]; then
                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Teamspeak server has been started!"
                    exit
                fi

                if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                    reset
                    show_logo

                    echo " "
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while fixing the server!"
                    echo
                    journalctl -p err -b -u teamspeak
                    exit
                fi
            done
            ;;
        --status)   # Get Status from Teamspeak server
            reset
            show_logo

            if [ $(systemctl show -p SubState --value teamspeak) == "start" ]; then
                echo " "
                echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Teamspeak server is starting!"
                echo
                exit
            fi

            if [ $(systemctl show -p SubState --value teamspeak) == "running" ]; then
                echo " "
                echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Teamspeak server is online!"
                echo
                exit
            fi

            if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                echo " "
                echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: Your Teamspeak server is offline!"
                echo
                exit
            fi
            ;;
        --live-log)   # Get Livelog from Teamspeak server
            reset
            show_logo

            journalctl -f -u teamspeak
            ;;
        --reboot)   # Reboot Teamspeak server
            reset
            show_logo
            echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Teamspeak server is restarting ..."
            
            echo -e "${COLOR_GREEN}Stop and restart teamspeak.service ...${COLOR_NC}"
            systemctl stop teamspeak.service
            sleep 2

            echo -e "${COLOR_GREEN}Release ports ...${COLOR_NC}"
            iptables -A INPUT -p udp --dport 9987 -j ACCEPT
            iptables -A INPUT -p tcp --dport 30033 -j ACCEPT
            iptables -A INPUT -p tcp --dport 10011 -j ACCEPT
            
            systemctl start teamspeak.service

            while true; do
                if [ $(systemctl show -p SubState --value teamspeak) == "running" ]; then
                    echo " "
                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Teamspeak server has been restarted!"
                    echo
                    exit
                fi

                if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                    reset
                    show_logo

                    echo " "
                    echo -e "[${COLOR_RED}ERROR${COLOR_NC}]: An error occurred while rebooting the server!"
                    echo
                    journalctl -p err -b -u teamspeak
                    exit
                fi
            done
            ;;
        --stop)   # Stop Teamspeak server
            reset
            show_logo

            while true; do
                if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                    echo " "
                    echo -e "[${COLOR_YELLOW}INFO${COLOR_NC}]: Your Teamspeak server is already stopped!"
                    echo
                    exit
                fi

                if [ $(systemctl show -p SubState --value teamspeak) == "running" ]; then
                    echo -e "${COLOR_GREEN}Stopping teamspeak.service ...${COLOR_NC}"
                    systemctl stop teamspeak.service
                    break
                fi
            done

            sleep 2

            while true; do
                if [ $(systemctl show -p SubState --value teamspeak) == "dead" ]; then
                    echo -e "[${COLOR_GREEN}SUCCESS${COLOR_NC}]: Your Teamspeak server has been stopped!"
                    exit
                fi
            done
            ;;
        -?*)
            reset
            show_logo
            show_help
            exit
            ;;
        *)
            break
    esac

    shift
done
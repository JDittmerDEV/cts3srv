=============================================================
                   CTS3SRV Compatibility
                  Copyright JDittmer.dev
                   https://jdittmer.dev
=============================================================

## Compatibilities checked

### Debian GNU/Linux
- Version 10.x
- Version 11.x
- Version 12.x (--ignore-os-check)

### Ubuntu
- Version 16.x
- Version 18.x
=============================================================
                    CTS3SRV Changelog
                  Copyright JDittmer.dev
                   https://jdittmer.dev
=============================================================
           + Added feature or noticeable improvement
           - Bug fix or something removed
           * Changed or Information
           ! Important - Take note!
=============================================================

## Release 1.1.1 17 September 2022

### Added
- Added "--status" function.
- Added "--live-log" function.
- Added "--server-query" function.


## Release 1.1.0 16 September 2022

### Added
- Added ASCII Logo
- Added "--ignore-os-check" function. This allows you to ignore server compatibility when creating, updating, fixing and removing the server.
- Added "--fix" function. This will rebuild the server. Recommended if there were big errors when creating or updating the server!
- Added "--start" function. This starts the server.
- Added "--reboot" function. This restarts the server.
- Added "--stop" function. This stops the server.
- Server compatibility is checked when creating, updating, fixing and removing the server. This can also be ignored during execution (--ignore-os-check).
- Some texts are now colored.

### Changed
- Help text revised.
- Many functions in "silent" mode.
- Some other minor changes.


## Release 1.0.2 13 September 2022

### Added
- Added "--update" function. It is checked for new updates and updated if necessary.
- Added "--prikey" function. The ServerAdmin privilege key is shown.

### Changed
- Spelling mistake corrected.
- If the Teamspeak server folder already exists during the installation, no attempt is made to create a new one.


## Release 1.0.1 13 September 2022

### Changed
- Find and download automatically latest Teamspeak version.